#!/bin/bash

[[ $EUID -ne 0 ]] && printf "Rerun as root or with sudo\n" && exit 1

ufw allow 5601/tcp
ufw allow 9200/tcp

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
apt install -y apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee /etc/apt/sources.list.d/elastic-7.x.list
apt update -y && apt install -y elasticsearch kibana zip unzip openssl

systemctl daemon-reload
systemctl enable --now elasticsearch.service
systemctl enable --now kibana.service

mkdir -p /etc/ssl/kibana
openssl genrsa -out /etc/ssl/kibana/kibana-key.key 2048
openssl req -new -x509 -sha512 -key /etc/ssl/kibana/kibana-key.key -out /etc/ssl/kibana/kibana-ca.crt -subj "/CN=$(hostname)"
openssl req -new -key /etc/ssl/kibana/kibana-key.key -out /etc/ssl/kibana/kibana-ca.csr -subj "/CN=$(hostname)"
openssl x509 -req -in /etc/ssl/kibana/kibana-ca.csr -CA /etc/ssl/kibana/kibana-ca.crt -CAkey /etc/ssl/kibana/kibana-key.key -CAcreateserial -sha512 -out /etc/ssl/kibana/kibana-cert.crt -days 3650
chown -R kibana: /etc/ssl/kibana

LOCAL_IP=$(ip r get 8.8.8.8 | grep -oP 'src \K[^ ]+')

cp /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.bak
cat << EOF | tee /etc/elasticsearch/elasticsearch.yml
network.host: $LOCAL_IP
http.port: 9200
discovery.type: single-node
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
xpack.security.enabled: true
xpack.security.authc.api_key.enabled: true
xpack:
  security:
    authc:
      realms:
        native:
          native1:
            order: 0
EOF

systemctl restart elasticsearch.service
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto -b | tee /root/elastic_passwords.txt
KIBANA_SYSTEM_PASSWORD=$(cat /root/elastic_passwords.txt | grep -i 'kibana_system =' | cut -d= -f2 | awk '{print $1}')

cp /etc/kibana/kibana.yml /etc/kibana/kibana.yml.bak
cat << EOF | tee /etc/kibana/kibana.yml
server.name: "$(hostname)"
server.host: "$LOCAL_IP"
server.port: 5601
elasticsearch.hosts: ["http://$LOCAL_IP:9200"]
elasticsearch.username: "kibana_system"
elasticsearch.password: "$KIBANA_SYSTEM_PASSWORD"
elasticsearch.ssl.certificateAuthorities: ["/etc/ssl/kibana/kibana-ca.crt"]
xpack.security.enabled: true
xpack.ingestManager.fleet.tlsCheckDisabled: true
xpack.encryptedSavedObjects.encryptionKey: "$(openssl rand -hex 32)"
server.ssl.certificate: "/etc/ssl/kibana/kibana-cert.crt"
server.ssl.key: "/etc/ssl/kibana/kibana-key.key"
server.ssl.enabled: true
EOF

systemctl restart elasticsearch.service
systemctl restart kibana.service
