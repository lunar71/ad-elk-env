[CmdletBinding()]
Param (
    [Parameter(Mandatory=$true)]
    $ELKServer,

    [Parameter(Mandatory=$true)]
    $ELKUsername,

    [Parameter(Mandatory=$true)]
    $ELKPassword,
)

New-Item -Path "C:\Setup" -ItemType "Directory"
Add-MpPreference -ExclusionPath "C:\Setup"

(New-Object System.Net.WebClient).DownloadFile("https://download.sysinternals.com/files/Sysmon.zip", "C:\Setup\Sysmon.zip")
Expand-Archive -LiteralPath C:\Setup\Sysmon.zip -DestinationPath C:\Setup\Sysmon
C:\Setup\Sysmon\sysmon64.exe -accepteula -i sysmonconfig-export.xml

(New-Object System.Net.WebClient).DownloadFile("https://artifacts.elastic.co/downloads/beats/winlogbeat/winlogbeat-7.13.2-windows-x86_64.zip", "C:\Setup\winlogbeat-7.13.2-windows-x86_64.zip")
Expand-Archive -LiteralPath C:\Setup\winlogbeat-7.13.2-windows-x86_64.zip -DestinationPath "C:\Program Files\Winlogbeat"
powershell -ExecutionPolicy bypass -File "C:\Program Files\Winlogbeat\install-service-winlogbeat.ps1"

$winlogbeat_yml = @"
winlogbeat.event_logs:
  - name: Application
    ignore_older: 72h
  - name: System
  - name: Security
    processors:
      - script:
          lang: javascript
          id: security
          file: ${path.home}/module/security/config/winlogbeat-security.js
  - name: Microsoft-Windows-Sysmon/Operational
    processors:
      - script:
          lang: javascript
          id: sysmon
          file: ${path.home}/module/sysmon/config/winlogbeat-sysmon.js
  - name: Windows PowerShell
    event_id: 400, 403, 600, 800
    processors:
      - script:
          lang: javascript
          id: powershell
          file: ${path.home}/module/powershell/config/winlogbeat-powershell.js
  - name: Microsoft-Windows-PowerShell/Operational
    event_id: 4103, 4104, 4105, 4106
    processors:
      - script:
          lang: javascript
          id: powershell
          file: ${path.home}/module/powershell/config/winlogbeat-powershell.js
  - name: ForwardedEvents
    tags: [forwarded]
    processors:
      - script:
          when.equals.winlog.channel: Security
          lang: javascript
          id: security
          file: ${path.home}/module/security/config/winlogbeat-security.js
      - script:
          when.equals.winlog.channel: Microsoft-Windows-Sysmon/Operational
          lang: javascript
          id: sysmon
          file: ${path.home}/module/sysmon/config/winlogbeat-sysmon.js
      - script:
          when.equals.winlog.channel: Windows PowerShell
          lang: javascript
          id: powershell
          file: ${path.home}/module/powershell/config/winlogbeat-powershell.js
      - script:
          when.equals.winlog.channel: Microsoft-Windows-PowerShell/Operational
          lang: javascript
          id: powershell
          file: ${path.home}/module/powershell/config/winlogbeat-powershell.js
setup.template.settings:
  index.number_of_shards: 1
setup.kibana:
  host: "$ELKServer:5601"
output.elasticsearch:
  hosts: ["$ELKServer:9200"]
  protocol: "http"
  username: "elastic"
  password: "$ELKPassword"
processors:
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
"@
[IO.File]::WriteALlLines("C:\Program Files\Winlogbeat\winlogbeat.yml", $winlogbeat_yml)
& 'C:\Program Files\Winlogbeat\winlogbeat.exe' setup -e -c "C:\Program Files\Winlogbeat\winlogbeat.yml"
Start-Service winlogbeat


if ((Get-CimInstance -ClassName Win32_OperatingSystem).ProductType -eq 2) {
    New-NetIPAddress `
        -IPAddress 192.168.1.112 `
        -DefaultGateway 192.168.1.1 `
        -PrefixLength 24 `
        -InterfaceAlias "Ethernet0"

    Expand-Archive `
        -Path DC-Advanced-Logging-GPO.zip `
        -DestinationPath C:\GPO\DC-Advanced-Logging-GPO `
        -Force

    $GPOS = Get-ChildItem C:\GPO\DC-Advanced-Logging-GPO | Where-Object Name -ne manifest.xml

    Foreach ($g in $GPOS) {
        $GPOName = (([XML]$x = Get-Content "$($g.FullName)\gpreport.xml").GPO).Name
        Import-GPO `
            -BackupGPOName $GPOName `
            -TargetName $GPOName `
            -Path $(Split-Path $g.FullName) `
            -CreateIfNeeded

        New-GPLink -Target "DC=LAB,DC=LOCAL" `
            -Name "$GPOName" `
            -LinkEnabled Yes `
            -Enforced Yes `
            -Order 1
    }
} else {
    New-NetIPAddress -IPAddress 192.168.1.113 -DefaultGateway 192.168.1.1 -PrefixLength 24 -InterfaceAlias "Ethernet0"
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet0" -ServerAddresses ("192.168.1.112","8.8.8.8")
    Add-Computer -DomainName "LAB.LOCAL" -Restart -Force
}
